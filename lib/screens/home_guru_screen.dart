import 'package:flutter/material.dart';

class HomeGuruScreen extends StatelessWidget {
  const HomeGuruScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color.fromARGB(255, 233, 84, 116),
      body: Center(
        child: Text('Home Tampilan Guru Screen'),
      ),
    );
  }
}
