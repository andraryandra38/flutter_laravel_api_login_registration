import 'package:flutter/material.dart';

class HomeSiswaScreen extends StatelessWidget {
  const HomeSiswaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color.fromARGB(255, 133, 200, 255),
      body: Center(
        child: Text('Home Tampilan Siswa Screen'),
      ),
    );
  }
}
